var BufferLoader = function (i, e, t) {
  (this.context = i),
    (this.arrUrl = e),
    (this.functionFinished = t),
    (this.arrBuffer = new Array()),
    (this.ind = 0);
};
(BufferLoader.prototype.load = function () {
  this.loadFile(0);
}),
  (BufferLoader.prototype.loadFile = function (i) {
    var e = new XMLHttpRequest(),
      t = this.arrUrl[i];
    e.open('GET', t, !0), (e.responseType = 'arraybuffer');
    var r = this.context,
      n = function () {};
    (e.onload = function () {
      r.decodeAudioData(
        e.response,
        function (i) {
          finishedFile(i);
        },
        n
      );
    }),
      e.send();
  }),
  (BufferLoader.prototype.finishedFile = function (i) {
    this.arrBuffer.push(i),
      this.ind >= this.arrUrl.length - 1
        ? this.functionFinished(this.arrBuffer)
        : (this.ind++, this.loadFile(this.ind));
  });
