var SLACK_LONG = 10,
  SLACK_SHORT = 10,
  DISTANCE_INSTANT_GRAB = 8,
  CURVATURE_RATIO = 0.45,
  OSCILLATION_SPEED_LOW_NOTES = 1.2,
  OSCILLATION_SPEED_HIGH_NOTES = 3,
  MINIMUM_AMPLITUDE = 7,
  AMPLITUDE_DAMPEN_LOW_NOTES = 0.92,
  AMPLITUDE_DAMPEN_HIGH_NOTES = 0.87,
  VOLUME_MIN = 0,
  VOLUME_MAX = 0;
const spanSound = document.querySelector('.music__switch');
const swither = document.querySelector('.toggle__fill');
swither.addEventListener(
  'click',
  () => (
    swither.classList.toggle('mute'),
    spanSound.textContent === 'Sound Off'
      ? (spanSound.textContent = 'Sound ON')
      : (spanSound.textContent = 'Sound Off'),
    swither.classList.contains('mute')
      ? ((VOLUME_MIN = 0.2), (VOLUME_MAX = 0.3))
      : ((VOLUME_MIN = 0), (VOLUME_MAX = 0)),
    VOLUME_MAX
  )
);
var PAN_LEFT = -0.3,
  PAN_RIGHT = 0.3,
  Thread = function (t, i, s, h, r, a) {
    (this.pt0 = new Point(0, 0)),
      (this.pt1 = new Point(0, 0)),
      (this.yp0 = this.yp1 = t),
      (this.pitchInd = i),
      (this.len = 0),
      this.xMid,
      this.yMid,
      this.xc,
      this.yc,
      this.xg,
      this.yg,
      this.xgi,
      this.ygi,
      this.xg1,
      this.yg1,
      this.xg0,
      this.yg0,
      this.xd,
      this.yd,
      this.dx,
      this.dy,
      (this.ampPxMin = 3),
      this.freq,
      this.ampDamp,
      this.distMax,
      this.distPerp,
      this.rGrab,
      this.rHalf,
      this.ang,
      this.angPerp,
      this.len,
      this.dx0,
      this.dy0,
      this.dx1,
      this.dy1,
      this.dist0,
      this.dist1,
      this.dxBez0,
      this.dyBez0,
      this.dxBez1,
      this.dyBez1,
      (this.ind = r),
      (this.str = s),
      (this.hex = h),
      (this.col = hex2rgb(this.hex)),
      (this.t = 0),
      this.ampStart,
      this.amp,
      this.ampMax,
      this.rStrength,
      (this.isUpdOn = !1),
      this.oscDir,
      (this.isGrabbed = !1),
      (this.isOsc = !1),
      (this.isFirstOsc = !1),
      (this.isShifting = !1),
      (this.carGrab = null),
      (this.cv = a),
      this.init();
  };
(Thread.prototype.init = function () {
  (this.m = suite.machine), this.updPos();
}),
  (Thread.prototype.setTargetPitch = function (t) {
    if (-1 == t) {
      this.pitchInd = -1;
      var i = 0;
    } else {
      (this.pitchInd = t), (this.rPitch = this.pitchInd / (TOTAL_NOTES - 1));
      i = this.m.arrLength[this.pitchInd];
    }
    i != this.len && this.easeToLength(i);
  }),
  (Thread.prototype.setLength = function (t) {
    (this.len = t), this.updPos();
  }),
  (Thread.prototype.easeToLength = function (t) {
    (this.isShifting = !0), (this.lenTarg = t);
  }),
  (Thread.prototype.updShifting = function () {
    var t = 0.2 * (this.lenTarg - this.len);
    Math.abs(t) < 1
      ? (this.setLength(this.lenTarg), (this.isShifting = !1))
      : this.setLength(this.len + t);
  }),
  (Thread.prototype.updPos = function () {
    (this.xp0 = -this.len / 2),
      (this.xp1 = this.len / 2),
      this.pt0.setX(this.xp0),
      this.pt0.setY(this.yp0),
      this.pt1.setX(this.xp1),
      this.pt1.setY(this.yp1),
      (this.dx = this.xp1 - this.xp0),
      (this.dy = this.yp1 - this.yp0),
      (this.xMid = this.xp0 + 0.5 * this.dx),
      (this.yMid = this.yp0 + 0.5 * this.dy),
      (this.ang = Math.atan2(this.dy, this.dx)),
      (this.angPerp = Math.PI / 2 - this.ang),
      (this.xc = this.xMid),
      (this.yc = this.yMid),
      (this.distMax = lerp(
        SLACK_SHORT,
        SLACK_LONG,
        (this.len - MIN_LENGTH) / (MAX_LENGTH - MIN_LENGTH)
      )),
      (this.freq = lerp(
        OSCILLATION_SPEED_LOW_NOTES,
        OSCILLATION_SPEED_HIGH_NOTES,
        this.rPitch
      )),
      (this.ampDamp = lerp(
        AMPLITUDE_DAMPEN_LOW_NOTES,
        AMPLITUDE_DAMPEN_HIGH_NOTES,
        this.rPitch
      )),
      (this.ampMax = this.distMax),
      (this.sinAng = Math.sin(this.ang)),
      (this.cosAng = Math.cos(this.ang)),
      (this.sinPerp = Math.sin(this.angPerp)),
      (this.cosPerp = Math.cos(this.angPerp));
  }),
  (Thread.prototype.redraw = function () {
    0 != this.len &&
      ((this.xo = this.m.xo),
      (this.yo = this.m.yo),
      isNaN(this.xp1) ||
        (this.cv.beginPath(),
        (this.cv.lineCap = 'round'),
        (this.cv.strokeStyle = this.hex),
        (this.cv.lineWidth = this.str),
        this.isGrabbed || this.isFirstOsc
          ? ((this.xd = this.xg), (this.yd = this.yg))
          : ((this.xd = this.xc), (this.yd = this.yc)),
        isNaN(this.xd) ||
          ((this.dx0 = this.xd - this.xp0),
          (this.dy0 = this.yd - this.yp0),
          (this.dx1 = this.xp1 - this.xd),
          (this.dy1 = this.yp1 - this.yd),
          (this.dist0 = Math.sqrt(this.dx0 * this.dx0 + this.dy0 * this.dy0)),
          (this.dist1 = Math.sqrt(this.dx1 * this.dx1 + this.dy1 * this.dy1)),
          (this.dxBez0 = CURVATURE_RATIO * this.dist0 * this.cosAng),
          (this.dyBez0 = CURVATURE_RATIO * this.dist0 * this.sinAng),
          (this.dxBez1 = CURVATURE_RATIO * this.dist1 * this.cosAng),
          (this.dyBez1 = CURVATURE_RATIO * this.dist1 * this.sinAng),
          this.cv.moveTo(this.xp0 + this.xo, this.yp0 + this.yo),
          this.cv.bezierCurveTo(
            this.xd - this.dxBez0 + this.xo,
            this.yd - this.dyBez0 + this.yo,
            this.xd + this.dxBez1 + this.xo,
            this.yd + this.dyBez1 + this.yo,
            this.xp1 + this.xo,
            this.yp1 + this.yo
          ),
          this.cv.stroke(),
          this.cv.closePath())));
  }),
  (Thread.prototype.upd = function () {
    this.isShifting && this.updShifting(),
      this.isGrabbed ? this.updGrab() : this.isOsc && this.updOsc();
  }),
  (Thread.prototype.updOsc = function () {
    if (this.isFirstOsc) {
      var t = this.xg1 - this.xg,
        i = this.yg1 - this.yg;
      if (
        ((this.xg += 0.8 * t),
        (this.yg += 0.8 * i),
        Math.abs(t) < 2 && Math.abs(i) < 2)
      )
        (this.t = 0),
          (this.oscDir = 1),
          (this.isFirstOsc = !1),
          sign(t) != sign(this.sinAng) && (this.oscDir *= -1);
    } else {
      this.t += this.freq * this.oscDir;
      var s = Math.sin(this.t);
      (this.amp *= this.ampDamp),
        (this.xc = this.xMid + s * this.sinAng * this.amp),
        (this.yc = this.yMid - s * this.cosAng * this.amp),
        this.amp <= 0.15 &&
          ((this.amp = 0), (this.isOsc = !1), this.m.checkMoving());
    }
  }),
  (Thread.prototype.updGrab = function () {
    if (null != this.carGrab)
      var t = this.carGrab.xp1,
        i = this.carGrab.yp1;
    else (t = this.m.getUserX()), (i = this.m.getUserY());
    var s = t - this.xp0,
      h = i - this.yp0,
      r = Math.atan2(h, s),
      a = this.ang - r,
      e = Math.sqrt(s * s + h * h);
    this.distPerp = e * Math.sin(a);
    var p = e * Math.cos(a);
    (this.rGrab = lim(p / this.len, 0, 1)),
      this.rGrab <= 0.5
        ? (this.rHalf = this.rGrab / 0.5)
        : (this.rHalf = 1 - (this.rGrab - 0.5) / 0.5);
    var d = this.distMax * this.rHalf;
    (this.rStrength = lim(Math.abs(this.distPerp) / this.distMax, 0, 1)),
      Math.abs(this.distPerp) > d
        ? this.drop()
        : ((this.xg = t), (this.yg = i));
  }),
  (Thread.prototype.pluck = function (t, i, s, h) {
    (this.xgi = this.xg = t), (this.ygi = this.yg = i);
    var r = this.m.getUserX(),
      a = this.m.getUserY(),
      e = r - this.xp0,
      p = a - this.yp0,
      d = (this.xgi, this.xp0, this.ygi, this.yp0, Math.sqrt(e * e + p * p));
    (this.rGrab = lim(d / this.len, 0, 1)),
      this.rGrab <= 0.5
        ? (this.rHalf = this.rGrab / 0.5)
        : (this.rHalf = 1 - (this.rGrab - 0.5) / 0.5);
    var n = this.distMax * this.rHalf,
      x = s ? 1 : this.m.getSpdAvg();
    (this.distPerp = (1 - x) * n),
      (this.rStrength = s
        ? 1
        : lim(Math.abs(this.distPerp) / this.distMax, 0, 1)),
      this.distPerp < this.ampPxMin && (this.distPerp = this.ampPxMin),
      (this.xg = this.xgi + this.distPerp * this.cosPerp),
      (this.yg = this.ygi + this.distPerp * this.sinPerp),
      (this.xc = this.xMid),
      (this.yc = this.yMid),
      this.isOsc
        ? ((this.rStrength = lim(
            0.5 * this.rStrength + this.amp / this.ampMax,
            0,
            1
          )),
          (this.amp = this.rStrength * this.ampMax))
        : ((this.amp = this.rStrength * this.ampMax), this.startOsc());
    var c = this.m.xAsRatio(t);
    this.playNote(this.rStrength, c, !1);
  }),
  (Thread.prototype.grab = function (t, i, s, h) {
    s
      ? ((this.carGrab = h), (this.carGrab.thrGrab = this))
      : (this.carGrab = null),
      (this.xgi = this.xg = t),
      (this.ygi = this.yg = i),
      (this.isGrabbed = !0),
      this.m.ctGrab++,
      this.updGrab();
  }),
  (Thread.prototype.drop = function () {
    this.m.ctGrab--,
      (this.isGrabbed = !1),
      (this.xc = this.xMid),
      (this.yc = this.yMid),
      (this.amp = this.rStrength * this.ampMax),
      null != this.carGrab &&
        ((this.carGrab.thrGrab = null), (this.carGrab = null));
    var t = this.m.xAsRatio(this.xg + this.xo);
    this.playNote(this.rStrength, t), this.startOsc();
  }),
  (Thread.prototype.startOsc = function () {
    (this.xg1 = this.xp0 + this.rGrab * this.dx),
      (this.yg1 = this.yp0 + this.rGrab * this.dy),
      (this.xg0 = this.xg),
      (this.yg0 = this.yg),
      (this.t = 0),
      (this.isFirstOsc = this.isOsc = !0);
  }),
  (Thread.prototype.playNote = function (t, i) {
    if (-1 != this.pitchInd) {
      this.pitchInd;
      suite.playSound(
        this.pitchInd,
        VOLUME_MIN + t * (VOLUME_MAX - VOLUME_MIN),
        PAN_LEFT + i * (PAN_RIGHT - PAN_LEFT)
      );
    }
  });
