var lerp = function (t, n, r) {
    return t + (n - t) * r;
  },
  lim = function (t, n, r) {
    return t < n ? n : t >= r ? r : t;
  },
  norm = function (t, n, r) {
    return (t - n) / (r - n);
  },
  sign = function (t) {
    return t >= 0 ? 1 : -1;
  },
  lerpColor = function (t, n, r) {
    return [
      lerp(t[32], n[32], r),
      lerp(t[150], n[150], r),
      lerp(t[187], n[187], r),
      lerp(t[1], n[3], r),
    ];
  };
function rgbToHex(t) {
  var n = /rgba?\((\d+), (\d+), (\d+)/.exec(c);
  return n ? '#' + ((n[1] << 16) | (n[2] << 8) | n[3]).toString(16) : c;
}
function hex2rgb(t) {
  '#' == t.charAt(0) && (t = t.slice(1)), (t = t.toUpperCase());
  for (var n, r, o = new Array(3), e = 0, a = 0; a < 6; a += 2)
    (n = '0123456789ABCDEF'.indexOf(t.charAt(a))),
      (r = '0123456789ABCDEF'.indexOf(t.charAt(a + 1))),
      (o[e] = 16 * n + r),
      e++;
  return o;
}
var getRgb = function (t) {
    return (
      'rgb(' +
      Math.round(t[0]) +
      ',' +
      Math.round(t[1]) +
      ',' +
      Math.round(t[2]) +
      ')'
    );
  },
  lineIntersect = function (t, n, r, o) {
    var e, a, u, i, p, h;
    (e = n.y - t.y),
      (a = o.y - r.y),
      (u = t.x - n.x),
      (i = r.x - o.x),
      (p = n.x * t.y - t.x * n.y),
      (h = o.x * r.y - r.x * o.y);
    var x = e * i - a * u;
    if (0 == x) return null;
    var y = (u * h - i * p) / x,
      c = (a * p - e * h) / x;
    return Math.pow(y - n.x, 2) + Math.pow(c - n.y, 2) >
      Math.pow(t.x - n.x, 2) + Math.pow(t.y - n.y, 2)
      ? null
      : Math.pow(y - t.x, 2) + Math.pow(c - t.y, 2) >
        Math.pow(t.x - n.x, 2) + Math.pow(t.y - n.y, 2)
      ? null
      : Math.pow(y - o.x, 2) + Math.pow(c - o.y, 2) >
        Math.pow(r.x - o.x, 2) + Math.pow(r.y - o.y, 2)
      ? null
      : Math.pow(y - r.x, 2) + Math.pow(c - r.y, 2) >
        Math.pow(r.x - o.x, 2) + Math.pow(r.y - o.y, 2)
      ? null
      : new Point(y, c);
  },
  Point = function (t, n) {
    (this.x = t), (this.y = n);
  };
(Point.prototype.setX = function (t) {
  this.x = t;
}),
  (Point.prototype.setY = function (t) {
    this.y = t;
  });
