var arrBuffers,
  arrUrl,
  suite = {},
  UPDATE_INTERVAL = 33,
  LOAD_UPDATE_INTERVAL = 80,
  TOTAL_NOTES = 38,
  BPM_NORM = 145,
  MOUSE_SPEED_MIN = 70,
  MOUSE_SPEED_MAX = 1500,
  MOUSE_SPEED_RATIO_GRAB = 0.4,
  MOUSE_AVERAGE_COUNT = 5,
  TOTAL_THREADS = 8,
  TOTAL_NOTES_IN_SONG = SONG_DATA_ARRAY.length,
  NOTE_UNIT = 2,
  HALF_STEP_MULTIPLIER = 0.943874312681769,
  MAX_LENGTH = 590,
  MIN_LENGTH = MAX_LENGTH * Math.pow(HALF_STEP_MULTIPLIER, TOTAL_NOTES - 1),
  MATH_PI = Math.PI,
  SHOW_FRAMERATE = !1,
  mouseX = 0,
  mouseY = 0,
  SPD_GRAB = 4,
  SPD_IGNORE_MAX = 80,
  MIDI_MAP = {
    36: 0,
    37: 1,
    38: 2,
    39: 3,
    40: 4,
    41: 5,
    42: 6,
    43: 7,
    44: 8,
    45: 9,
    46: 10,
    47: 11,
    48: 12,
    49: 13,
    50: 14,
    51: 15,
    52: 16,
    53: 17,
    54: 18,
    55: 19,
    56: 20,
    57: 21,
    58: 22,
    59: 23,
    60: 24,
    61: 25,
    62: 26,
    63: 27,
    64: 28,
    65: 29,
    66: 30,
    67: 31,
    68: 32,
    69: 33,
    70: 34,
    71: 35,
    72: 36,
  },
  context = null;
(suite.everythingIsReady = function () {
  suite.ready || ((suite.ready = !0), suite.machine.doneLoading());
}),
  (suite.init = function () {
    var e;
    (suite.ready = !1),
      suite.initMidiMap(),
      (contextClass =
        window.AudioContext ||
        window.webkitAudioContext ||
        window.mozAudioContext ||
        window.oAudioContext ||
        window.msAudioContext),
      contextClass && (context = new contextClass()),
      window.addEventListener('resize', rsize, !1),
      document.addEventListener(
        'mousemove',
        function (e) {
          (mouseX = e.pageX), (mouseY = e.pageY);
        },
        !1
      ),
      document.addEventListener(
        'mousedown',
        function (e) {
          (mousePressed = !0),
            null != suite.machine.mouseDown && suite.machine.mouseDown(e),
            e.preventDefault();
        },
        !1
      ),
      document.addEventListener(
        'mouseup',
        function (e) {
          (mousePressed = !1),
            null != suite.machine.mouseUp && suite.machine.mouseUp(e);
        },
        !1
      ),
      (suite.canvasEl = document.getElementById('main-canvas')),
      (suite.canvasObj = suite.canvasEl.getContext('2d')),
      (suite.machine = new Machine(suite.canvasObj)),
      (suite.indNoteLd = 0),
      rsize(),
      suite.machine.build(),
      suite.machine.beginLoading(),
      setInterval(updateLoop, UPDATE_INTERVAL),
      (arrBuffers = new Array(TOTAL_NOTES)),
      (arrUrl = new Array(TOTAL_NOTES));
    for (var n = 0; n < TOTAL_NOTES; n++)
      (e = n < 10 ? '0' : ''), (arrUrl[n] = 'audio/harp_' + e + n + '.mp3');
    (bufferLoader = new BufferLoader(context, arrUrl, finishedLoading)),
      bufferLoader.load();
  }),
  (suite.initMidiMap = function () {
    var e;
    for (key in ((suite.arrMidiMap = new Array()), MIDI_MAP))
      (e = parseInt(key)), (suite.arrMidiMap[e] = MIDI_MAP[key]);
  });
var updateLoop = function () {
  suite.machine.upd();
};
function finishedFile(e) {
  bufferLoader.finishedFile(e), suite.indNoteLd++;
}
function finishedLoading(e) {
  (arrBuffers = e),
    (suite.soundAvailable = !0),
    (suite.soundReady = !0),
    suite.everythingIsReady();
}
(init = function () {
  suite.init();
}),
  (suite.playSound = function (e, n, i) {
    var t = arrBuffers[e],
      o = context.createBufferSource();
    o.buffer = t;
    var u = context.createGain();
    o.connect(u), u.connect(context.destination), (u.gain.value = n), o.start();
  });
var rsize = function () {
  (width = window.innerWidth),
    (height = window.innerHeight),
    null != suite.machine && suite.machine.rsize();
};
function getQueryVariable(e) {
  for (
    var n = window.location.search.substring(1).split('&'), i = 0;
    i < n.length;
    i++
  ) {
    var t = n[i].split('=');
    if (t[0] == e) return t[1];
  }
  return null;
}
window.console || (console = {}),
  (console.log = console.log || function () {}),
  (console.warn = console.warn || function () {}),
  (console.error = console.error || function () {}),
  (console.info = console.info || function () {});
