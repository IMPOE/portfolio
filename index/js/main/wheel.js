var Wheel = function (t, i, h, s) {
  (this.ind = h), (this.xp = t), (this.yp = i), this.setRot(0.25 * MATH_PI);
  var n = 0 == this.ind ? 1 : -1;
  (this.rotSpd = 0.035 * n), (this.cv = s), this.init();
};
(Wheel.prototype.init = function () {
  this.m = suite.machine;
}),
  (Wheel.prototype.upd = function () {
    (this.sinAng = Math.sin(this.rot)),
      (this.cosAng = Math.cos(this.rot)),
      this.nub0.upd(),
      this.nub1.upd();
  }),
  (Wheel.prototype.redraw = function () {
    this.nub0.redraw(), this.nub1.redraw();
  }),
  (Wheel.prototype.setRot = function (t) {
    this.rot = t;
  });
