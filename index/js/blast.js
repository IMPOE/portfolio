!(function (e, t, a, r) {
  var n = (function () {
      if (a.documentMode) return a.documentMode;
      for (var e = 7; e > 0; e--) {
        var t = a.createElement('div');
        if (
          ((t.innerHTML =
            '\x3c!--[if IE ' + e + ']><span></span><![endif]--\x3e'),
          t.getElementsByTagName('span').length)
        )
          return (t = null), e;
        t = null;
      }
      return r;
    })(),
    i = t.console || { log: function () {}, time: function () {} },
    s = 'blast',
    d = 'вЂ“вЂ”вЂІвЂ™\'вЂњвЂівЂћ"(В«.вЂ¦ВЎВївЂІвЂ™\'вЂќвЂівЂњ")В».вЂ¦!?',
    l =
      '\\u0041-\\u005A\\u0061-\\u007A\\u00C0-\\u017F\\u0100-\\u01FF\\u0180-\\u027F',
    o = {
      abbreviations: new RegExp(
        '[^' +
          l +
          '](e\\.g\\.)|(i\\.e\\.)|(mr\\.)|(mrs\\.)|(ms\\.)|(dr\\.)|(prof\\.)|(esq\\.)|(sr\\.)|(jr\\.)[^' +
          l +
          ']',
        'ig'
      ),
      innerWordPeriod: new RegExp('[' + l + '].[' + l + ']', 'ig'),
      onlyContainsPunctuation: new RegExp('[^' + d + ']'),
      adjoinedPunctuation: new RegExp('^[' + d + ']+|[' + d + ']+$', 'g'),
      skippedElements: /(script|style|select|textarea)/i,
      hasPluginClass: new RegExp('(^| )' + s + '( |$)', 'gi'),
    };
  (e.fn[s] = function (d) {
    function c(e) {
      return e.replace(/{{(\d{1,3})}}/g, function (e, t) {
        return String.fromCharCode(t);
      });
    }
    function u(e, t) {
      var r = -1,
        n = 0;
      if (3 === e.nodeType && e.data.length) {
        if (
          (m.nodeBeginning &&
            ((e.data =
              t.search || 'sentence' !== t.delimiter
                ? c(e.data)
                : e.data
                    .replace(o.abbreviations, function (e) {
                      return e.replace(/\./g, '{{46}}');
                    })
                    .replace(o.innerWordPeriod, function (e) {
                      return e.replace(/\./g, '{{46}}');
                    })),
            (m.nodeBeginning = !1)),
          -1 !== (r = e.data.search(g)))
        ) {
          var i = e.data.match(g),
            d = i[0],
            l = i[1] || !1;
          '' === d ? r++ : l && l !== d && ((r += d.indexOf(l)), (d = l));
          var p = e.splitText(r);
          p.splitText(d.length),
            (n = 1),
            t.search || 'sentence' !== t.delimiter || (p.data = c(p.data));
          var h = (function (e, t) {
            var r = a.createElement(t.tag);
            if (
              ((r.className = s),
              t.customClass &&
                ((r.className += ' ' + t.customClass),
                t.generateIndexID &&
                  (r.id = t.customClass + '-' + m.blastedIndex)),
              'all' === t.delimiter &&
                /\s/.test(e.data) &&
                (r.style.whiteSpace = 'pre-line'),
              !0 === t.generateValueClass &&
                !t.search &&
                ('character' === t.delimiter || 'word' === t.delimiter))
            ) {
              var n,
                i = e.data;
              'word' === t.delimiter &&
                o.onlyContainsPunctuation.test(i) &&
                (i = i.replace(o.adjoinedPunctuation, '')),
                (n =
                  s + '-' + t.delimiter.toLowerCase() + '-' + i.toLowerCase()),
                (r.className += ' ' + n);
            }
            return (
              t.aria && r.setAttribute('aria-hidden', 'true'),
              r.appendChild(e.cloneNode(!1)),
              r
            );
          })(p, t, m.blastedIndex);
          p.parentNode.replaceChild(h, p), m.wrappers.push(h), m.blastedIndex++;
        }
      } else if (
        1 === e.nodeType &&
        e.hasChildNodes() &&
        !o.skippedElements.test(e.tagName) &&
        !o.hasPluginClass.test(e.className)
      )
        for (var f = 0; f < e.childNodes.length; f++)
          (m.nodeBeginning = !0), (f += u(e.childNodes[f], t));
      return n;
    }
    var g,
      p = e.extend({}, e.fn[s].defaults, d),
      m = {};
    if (
      p.search.length &&
      ('string' == typeof p.search || /^\d/.test(parseFloat(p.search)))
    )
      (p.delimiter = p.search
        .toString()
        .replace(/[-[\]{,}(.)*+?|^$\\\/]/g, '\\$&')),
        (g = new RegExp(
          '(?:^|[^-' + l + '])(' + p.delimiter + "('s)?)(?![-" + l + '])',
          'i'
        ));
    else
      switch (
        ('string' == typeof p.delimiter &&
          (p.delimiter = p.delimiter.toLowerCase()),
        p.delimiter)
      ) {
        case 'all':
          g = /(.)/;
          break;
        case 'letter':
        case 'char':
        case 'character':
          g = /(\S)/;
          break;
        case 'word':
          g = /\s*(\S+)\s*/;
          break;
        case 'sentence':
          g = /(?=\S)(([.]{2,})?[^!?]+?([.вЂ¦!?]+|(?=\s+$)|$)(\s*[вЂІвЂ™'вЂќвЂівЂњ")В»]+)*)/;
          break;
        case 'element':
          g = /(?=\S)([\S\s]*\S)/;
          break;
        default:
          if (!(p.delimiter instanceof RegExp))
            return (
              i.log(
                s +
                  ': Unrecognized delimiter, empty search string, or invalid custom Regex. Aborting.'
              ),
              !0
            );
          g = p.delimiter;
      }
    function h(a, d) {
      d.debug && i.time('blast reversal');
      var l = !1;
      a
        .removeClass(s + '-root')
        .removeAttr('aria-label')
        .find('.' + s)
        .each(function () {
          if (e(this).closest('.' + s + '-root').length) l = !0;
          else {
            var t = this.parentNode;
            n <= 7 && t.firstChild.nodeName,
              t.replaceChild(this.firstChild, this),
              t.normalize();
          }
        }),
        t.Zepto ? a.data(s, r) : a.removeData(s),
        d.debug &&
          (i.log(
            s +
              ': Reversed Blast' +
              (a.attr('id') ? ' on #' + a.attr('id') + '.' : '.') +
              (l
                ? ' Skipped reversal on the children of one or more descendant root elements.'
                : '')
          ),
          i.timeEnd('blast reversal'));
    }
    if (
      (this.each(function () {
        var t = e(this),
          n = t.text();
        if (!1 !== d) {
          (m = {
            blastedIndex: 0,
            nodeBeginning: !1,
            wrappers: m.wrappers || [],
          }),
            t.data(s) === r ||
              ('search' === t.data(s) && !1 !== p.search) ||
              (h(t, p),
              p.debug && i.log(s + ": Removed element's existing Blast call.")),
            t.data(s, !1 !== p.search ? 'search' : p.delimiter),
            p.aria && t.attr('aria-label', n),
            p.stripHTMLTags && t.html(n);
          try {
            a.createElement(p.tag);
          } catch (e) {
            (p.tag = 'span'),
              p.debug &&
                i.log(s + ': Invalid tag supplied. Defaulting to span.');
          }
          t.addClass(s + '-root'),
            p.debug && i.time(s),
            u(this, p),
            p.debug && i.timeEnd(s);
        } else !1 === d && t.data(s) !== r && h(t, p);
        p.debug &&
          e.each(m.wrappers, function (e, t) {
            i.log(s + ' [' + p.delimiter + '] ' + this.outerHTML),
              (this.style.backgroundColor = e % 2 ? '#f12185' : '#075d9a');
          });
      }),
      !1 !== d && !0 === p.returnGenerated)
    ) {
      var f = e().add(m.wrappers);
      return (f.prevObject = this), (f.context = this.context), f;
    }
    return this;
  }),
    (e.fn.blast.defaults = {
      returnGenerated: !0,
      delimiter: 'word',
      tag: 'span',
      search: !1,
      customClass: '',
      generateIndexID: !1,
      generateValueClass: !1,
      stripHTMLTags: !1,
      aria: !0,
      debug: !1,
    });
})(window.jQuery || window.Zepto, window, document);
