$(function () {
  const chars = $('h1').blast({
    delimiter: 'character',
  });
  chars.each(function (i) {
    $(this)
      .css({
        position: 'relative',
        left: 0,
      })
      .delay(i * 45)
      .animate({ left: '50px' }, 300);
  });
});
