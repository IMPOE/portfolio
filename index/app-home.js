let home = {
  waypoints: [],
  init: function () {
    jQuery('h1,h2').blast({
      delimiter: 'character',
      tag: 'span',
    });

    this.initSection_home();
    this.initWaypoints();
  },
  destroy: function () {
    if (typeof initOciliator !== 'undefined') {
      initOciliator(true);
    }

    jQuery('script#oscil').remove();
    home.waypoints.forEach(function (element, index, array) {
      element.destroy();
    });

    jQuery.colorbox.remove();
    jQuery('script#tagcanvas').remove();
    jQuery('#myCanvas').remove();

    if (typeof app.contactForm !== 'undefined') {
      app.contactForm.destroy();
    }
  },
  initWaypoints: function () {
    [].slice.call(document.querySelectorAll('.js-way')).forEach(function (el) {
      home.waypoints.push(
        new Waypoint({
          element: el,
          handler: function (direction) {
            home['initSection_' + jQuery(this.element).attr('data-load')]();

            this.destroy();
          },
          offset: '80%',
        })
      );
    });
  },
  initSection_home: function () {
    let sectionHome = jQuery('#section-home');
    sectionHome.addClass('loaded');

    let a = 0;
    let b = 0;
    sectionHome.find('.blast').each(function () {
      if (a == 300) {
        a = 400;
      }

      if (a == 1100) {
        a = 1200;
      }

      let el = $(this);
      if (a == 400) {
        setTimeout(function () {
          sectionHome.find('h1 img').addClass('animation-logo');
        }, 800);
      }

      setTimeout(function () {
        el.addClass('animated bounceIn');
      }, a);

      if (a < 1200) {
        a = a + 100;
      } else {
        a = a + 80;
      }
    });
    setTimeout(function () {
      sectionHome.find('.blast').removeClass('animated bounceIn');
      sectionHome.find('.blast').css('opacity', 1);

      sectionHome.find('.blast').mouseenter(function () {
        let el = jQuery(this);

        jQuery(this).addClass('animated rubberBand');
        jQuery(this).one(
          'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
          function () {
            el.removeClass('animated rubberBand');
          }
        );
      });
    }, 3000);
  },
  initSection_about: function () {
    let myScript = document.createElement('script');
    myScript.setAttribute('src', path + 'js/vendor/tagcanvas.js');
    myScript.setAttribute('id', 'tagcanvas');
    document.body.appendChild(myScript);

    myScript.addEventListener(
      'load',
      function (e) {
        if (
          !$('#myCanvas').tagcanvas({
            textColour: '#08FDD8',
            outlineThickness: 0.5,
            outlineColour: '#FE0853',
            maxSpeed: 0.06,
            freezeActive: true,
            shuffleTags: true,
            shape: 'sphere',
            zoom: 0.8,
            noSelect: true,
            textFont: null,
            pinchZoom: true,
            wheelZoom: false,
            freezeDecel: true,
            fadeIn: 3000,
            initial: [0.3, -0.1],
            depth: 1.1,
          })
        ) {
        }
      },
      false
    );
  },
};
home.init();
