let work = {
  magicwall: null,
  init: function () {
    let myScript = document.createElement('script');
    myScript.setAttribute('src', path + 'js/vendor/magicwall.js');
    myScript.setAttribute('id', 'magicwall');
    document.body.appendChild(myScript);

    myScript.addEventListener(
      'load',
      function (e) {
        work.magicwall = jQuery('.magicwall').magicWall({
          maxItemHeight: 350,
          maxItemWidth: 350,
          delay: 400,
          preloadBeforeSwitch: true,
          loadingMode: 'chain',
          pauseOnHover: 'item',
          animations:
            'flipY,rollOutX,-rollOutX,rollOutY,-rollOutY,slideColumn,-slideColumn,slideRow,-slideRow,fade',
          duration: 800,
        });

        jQuery('.colorbox').colorbox({
          maxWidth: '70%',
          maxHeight: '250%',
          scrolling: true,
          onOpen: function () {
            jQuery('.magicwall').magicWall('stop');
          },

          onClosed: function () {
            jQuery('.magicwall').magicWall('start');
          },
        });
      },
      false
    );
  },
  destroy: function () {
    jQuery('script#magicwall').remove();
    jQuery.colorbox.remove();
    work.magicwall.data('_magicWallInstance').destroy();
  },
};

work.init();
