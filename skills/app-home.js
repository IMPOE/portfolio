let home = {
  initSection_about: function () {
    let myScript = document.createElement('script');
    myScript.setAttribute('src', path + 'js/vendor/tagcanvas.js');
    myScript.setAttribute('id', 'tagcanvas');
    document.body.appendChild(myScript);

    myScript.addEventListener(
      'load',
      function (e) {
        if (
          !$('#myCanvas').tagcanvas({
            textColour: '#08FDD8',
            outlineThickness: 0.5,
            outlineColour: '#FE0853',
            maxSpeed: 0.06,
            freezeActive: true,
            shuffleTags: true,
            shape: 'sphere',
            zoom: 0.8,
            noSelect: true,
            textFont: null,
            pinchZoom: true,
            wheelZoom: false,
            freezeDecel: true,
            fadeIn: 3000,
            initial: [0.3, -0.1],
            depth: 1.1,
          })
        ) {
        }
      },
      false
    );
  },
};
home.initSection_about();
